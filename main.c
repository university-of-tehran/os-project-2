#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <dirent.h>

int countFiles(char *directory)
{
  int fileCount = 0;
  DIR *dirp;
  struct dirent *entry;
  dirp = opendir(directory);
  while ((entry = readdir(dirp)) != NULL)
  {
    if (entry->d_type == DT_REG)
    {
      fileCount++;
    }
  }
  closedir(dirp);
  return fileCount;
}

float getfield(char *line, int num)
{
  const char *tok;
  for (tok = strtok(line, ";");
       tok && *tok;
       tok = strtok(NULL, ";\n"))
  {
    if (!--num)
    {
      return atof(tok);
    }
  }
  return 0;
}

void filesArray(char *directory, char **filesList)
{
  int n = 0, i = 0;
  DIR *d;
  struct dirent *dir;
  d = opendir(directory);

  //Determine the number of files
  while ((dir = readdir(d)) != NULL)
  {
    if (!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, ".."))
    {
    }
    else
    {
      n++;
    }
  }
  rewinddir(d);

  while ((dir = readdir(d)) != NULL)
  {
    if (!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, ".."))
    {
    }
    else
    {
      filesList[i] = (char *)malloc(100);
      sprintf(filesList[i], "%s%s%s", directory, "/", dir->d_name);
      i++;
    }
  }
  rewinddir(d);
}

void readVectorOfPredictor(char *fileName, float **predictorvectors, int *lineNumber, int *fieldNumber)
{
  FILE *stream = fopen(fileName, "r");
  char line[1024] = {'\0'};
  fgets(line, 1024, stream);
  while (fgets(line, 1024, stream))
  {
    char *tmp = strdup(line);
    for (*fieldNumber = 0; *fieldNumber < 50; *fieldNumber++)
    {
      if (getfield(tmp, *fieldNumber))
      {
        predictorvectors[*lineNumber][*fieldNumber] = getfield(tmp, *fieldNumber);
      }
      else
      {
        break;
      }
    }
    free(tmp);
    *lineNumber++;
  }
}

void readDataset(char *fileName, float **datasetData, int *lineNumber, int *fieldNumber)
{
  FILE *stream = fopen(fileName, "r");
  char line[1024] = {'\0'};
  fgets(line, 1024, stream);
  while (fgets(line, 1024, stream))
  {
    char *tmp = strdup(line);
    for (*fieldNumber = 0; *fieldNumber < 50; *fieldNumber++)
    {
      if (getfield(tmp, *fieldNumber))
      {
        datasetData[*lineNumber][*fieldNumber] = getfield(tmp, *fieldNumber);
      }
      else
      {
        break;
      }
    }
    free(tmp);
    *lineNumber++;
  }
}

void calculatePredictorResult(float **predictorvectors, char *predictorResult, char *datasetFile)
{
  float datasetData[3000][50];
  int lineNumber = 0;
  int temp[50] = {0};
  int fieldNumber = 0;
  readDataset(datasetFile, datasetData, lineNumber, fieldNumber);
  // for (int i = 0; i < lineNumber; i++)
  // {
  //   for (int j = 0; j < 50; j++)
  //   {
  //     for (int p = 0; p < 50; p++)
  //     {
  //       temp[j] +=
  //     }
  //   }

  //   temp = { 0 }
  // }
}

int main(int argc, char **argv)
{
  if (argc < 3)
  {
    write(1, "Not enough arguments", sizeof("Not enough arguments"));
  }
  char *validationDir = argv[1];
  char *vectorsDir = argv[2];
  int vectorsCount = countFiles(vectorsDir);
  int validationsCount = countFiles(validationDir);
  char *vectorFiles[1000];
  char *validationFiles[1000];
  filesArray(validationDir, validationFiles);
  filesArray(vectorsDir, vectorFiles);

  int voterPipe[2], predictorPipe[2], resultPipe[2];
  int voterPid = fork();
  int predictorPid = 0;
  int exitStatus;
  char finalResult[100];

  if (pipe(voterPipe) < 0 || pipe(predictorPipe) < 0 || pipe(resultPipe) < 0)
    exit(1);

  if (voterPid != 0)
  { // Main
    for (int i = 0; i < vectorsCount; i++)
    {
      predictorPid = fork();
      if (predictorPid != 0)
      { // Main
      }
      else
      { // Predictor
        // This predictor vectors is in vectorFiles[i]
        // Knows the validation files
        // Knows the voterPipe
        float predictorvectors[50][50] = {{0}};
        char predictorResult[3000] = {'\0'};
        int lineNumber = 0;
        int fieldNumber = 0;
        readVectorOfPredictor(vectorFiles[i], predictorvectors, &lineNumber, &fieldNumber);
        calculatePredictorResult(predictorvectors, predictorResult, validationFiles[0]);
        write(voterPipe[1], predictorResult, sizeof(predictorResult));
      }
    }
    // Wait for the voter to give you the results
    read(resultPipe[0], finalResult, sizeof(finalResult));
    write(0, finalResult, sizeof(finalResult));
  }
  else
  { // Voter

    int intFinalResult = 0;
    char validString[3000] = {'\0'};
    char predictorsString[vectorsCount][3000];
    char predictedString[3000] = {'\0'};

    for (int i = 0; i < vectorsCount; i++)
    {
      read(voterPipe[0], predictorsString[i], sizeof(predictorsString[i]));
    }

    for (int i = 0; i < 3000; i++)
    {
    }

    sprintf(finalResult, "%d", intFinalResult);
    write(resultPipe[1], &finalResult, sizeof(finalResult));
  }

  wait(&exitStatus);
  Exit();
}